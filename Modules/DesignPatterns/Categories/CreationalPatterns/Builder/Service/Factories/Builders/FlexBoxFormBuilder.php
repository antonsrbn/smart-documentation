<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Builders;


use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\IFormBuilder;

/**
 * Class FlexBoxFormBuilder.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Builders
 */
class FlexBoxFormBuilder implements IFormBuilder
{
    private string $title;
    private array  $elements;

    public function __construct()
    {
        $this->title    = '';
        $this->elements = [];
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title): self
    {
        $this->title = "<FlexBoxHead>{$title}</FlexBoxHead>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addTextField(string $name, string $id, bool $isRequired = false): self
    {
        $this->elements[] = "<FlexBoxTextField id='{$id}' {$this->makeRequired($isRequired)}>{$name}</FlexBoxTextField>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addCheckboxField(string $name, string $id, bool $isRequired = false): self
    {
        $this->elements[] = "<FlexBoxCheckboxField id='{$id}' {$this->makeRequired($isRequired)}>{$name}</FlexBoxCheckboxField>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addTexAreaField(string $name, string $id, bool $isRequired = false): self
    {
        $this->elements[] = "<FlexBoxTexAreaField id='{$id}' {$this->makeRequired($isRequired)}>{$name}</FlexBoxTexAreaField>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function reset(): self
    {
        $this->title    = '';
        $this->elements = [];

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function make(): string
    {
        $result = '<FlexBoxForm>' . "\n";
        $result .= $this->title . "\n";

        foreach ($this->elements as $element) {
            $result .= $element . "\n";
        }

        $result .= $this->submitButton() . "\n";

        $result .= '</FlexBoxForm>';


        return $result;
    }

    /**
     * @param bool $required
     * @return string
     */
    private function makeRequired(bool $required): string
    {
        return $required ? 'required=\'true\'' : '';
    }

    private function submitButton(): string
    {
        return "<FlexBoxSubmitButton>Отправить</FlexBoxSubmitButton>";
    }
}
