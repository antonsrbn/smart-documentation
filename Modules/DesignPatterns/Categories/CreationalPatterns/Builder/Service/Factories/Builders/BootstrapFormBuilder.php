<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Builders;


use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\IFormBuilder;

/**
 * Class BootstrapFormBuilder.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Builders
 */
class BootstrapFormBuilder implements IFormBuilder
{
    private string $title;
    private array  $elements;

    public function __construct()
    {
        $this->title    = '';
        $this->elements = [];
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title): self
    {
        $this->title = "<BotstrapHead>{$title}</BotstrapHead>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addTextField(string $name, string $id, bool $isRequired = false): self
    {
        $this->elements[] = "<BotstrapTextField id='{$id}' {$this->makeRequired($isRequired)}>{$name}</BotstrapTextField>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addCheckboxField(string $name, string $id, bool $isRequired = false): self
    {
        $this->elements[] = "<BotstrapCheckboxField id='{$id}' {$this->makeRequired($isRequired)}>{$name}</BotstrapCheckboxField>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addTexAreaField(string $name, string $id, bool $isRequired = false): self
    {
        $this->elements[] = "<BotstrapTexAreaField id='{$id}' {$this->makeRequired($isRequired)}>{$name}</BotstrapTexAreaField>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function reset(): self
    {
        $this->title    = '';
        $this->elements = [];

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function make(): string
    {
        $result = '<BotstrapForm>' . "\n";
        $result .= $this->title . "\n";

        foreach ($this->elements as $element) {
            $result .= $element . "\n";
        }

        $result .= $this->submitButton() . "\n";

        $result .= '</BotstrapForm>';


        return $result;
    }

    /**
     * @param bool $required
     * @return string
     */
    private function makeRequired(bool $required): string
    {
        return $required ? 'required=\'true\'' : '';
    }

    private function submitButton(): string
    {
        return "<BotstrapSubmitButton>Отправить</BotstrapSubmitButton>";
    }
}
