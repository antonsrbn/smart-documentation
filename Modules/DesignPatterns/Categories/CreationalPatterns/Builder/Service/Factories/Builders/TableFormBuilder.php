<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Builders;


use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\IFormBuilder;

/**
 * Class TableFormBuilder.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Builders
 */
class TableFormBuilder implements IFormBuilder
{
    private string $title;
    private array  $elements;

    public function __construct()
    {
        $this->title    = '';
        $this->elements = [];
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title): self
    {
        $this->title = "<TableHead>{$title}</TableHead>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addTextField(string $name, string $id, bool $isRequired = false): self
    {
        $this->elements[] = "<TableTextField id='{$id}' {$this->makeRequired($isRequired)}>{$name}</TableTextField>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addCheckboxField(string $name, string $id, bool $isRequired = false): self
    {
        $this->elements[] = "<TableCheckboxField id='{$id}' {$this->makeRequired($isRequired)}>{$name}</TableCheckboxField>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addTexAreaField(string $name, string $id, bool $isRequired = false): self
    {
        $this->elements[] = "<TableTexAreaField id='{$id}' {$this->makeRequired($isRequired)}>{$name}</TableTexAreaField>";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function reset(): self
    {
        $this->title    = '';
        $this->elements = [];

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function make(): string
    {
        $result = '<TableForm>' . "\n";
        $result .= $this->title . "\n";

        foreach ($this->elements as $element) {
            $result .= $element . "\n";
        }

        $result .= $this->submitButton() . "\n";

        $result .= '</TableForm>';


        return $result;
    }

    /**
     * @param bool $required
     * @return string
     */
    private function makeRequired(bool $required): string
    {
        return $required ? 'required=\'true\'' : '';
    }

    private function submitButton(): string
    {
        return "<TableSubmitButton>Отправить</TableSubmitButton>";
    }
}
