<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors;


use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\IFormBuilder;

/**
 * Class ReviewFormDirector.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors
 */
class ReviewFormDirector
{
    /**
     * @inheritDoc
     */
    public function build(IFormBuilder $formBuilder): void
    {
        $formBuilder
            ->reset()
            ->setTitle('Оставьте отзыв')
            ->addTextField('Ваше имя', 'name', true)
            ->addTextField('Ваш email', 'email', true)
            ->addTexAreaField('Ваш отзыв', 'comments', true);
    }
}
