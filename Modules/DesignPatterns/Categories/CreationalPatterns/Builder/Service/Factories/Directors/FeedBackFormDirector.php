<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors;


use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\IFormBuilder;


/**
 * Class FeedBackFormDirector.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors
 */
class FeedBackFormDirector
{
    /**
     * @inheritDoc
     */
    public function build(IFormBuilder $formBuilder): void
    {
        $formBuilder
            ->reset()
            ->setTitle('Обратная связь')
            ->addTextField('Ваше имя', 'name', true)
            ->addTextField('Ваш телефон', 'phone', true)
            ->addTexAreaField('Ваш комментарий', 'comments', false)
            ->addCheckboxField('Вы соглашаетесь с условиями обработки персональных данных', 'personal-data',true);
    }
}
