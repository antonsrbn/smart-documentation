<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service;

/**
 * Interface IFormBuilder.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service
 */
interface IFormBuilder
{
    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): self;

    /**
     * @param string $name
     * @param string $id
     * @param false  $isRequired
     * @return $this
     */
    public function addTextField(string $name, string $id, bool $isRequired = false): self;

    /**
     * @param string $name
     * @param string $id
     * @param false  $isRequired
     * @return $this
     */
    public function addCheckboxField(string $name, string $id, bool $isRequired = false): self;

    /**
     * @param string $name
     * @param string $id
     * @param false  $isRequired
     * @return $this
     */
    public function addTexAreaField(string $name, string $id, bool $isRequired = false): self;

    /**
     * @return string
     */
    public function make(): string;

    /**
     * @return $this
     */
    public function reset(): self;
}
