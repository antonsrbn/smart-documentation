<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service;

use Exception;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Builders\BootstrapFormBuilder;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Builders\FlexBoxFormBuilder;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Builders\TableFormBuilder;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors\FeedBackFormDirector;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors\FeedBackFlexBoxFormDirector;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors\FeedBackTableFormDirector;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors\ReviewFormDirector;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors\ReviewFlexBoxFormDirector;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors\ReviewTableFormDirector;
use Modules\DesignPatterns\Service\IClientCode;
use Modules\DesignPatterns\Service\Report;

/**
 * Class BuilderClientCode
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service
 */
class BuilderClientCode implements IClientCode
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'builder';
    }

    /**
     * @inheritDoc
     */
    public function main(): void
    {
        $tableFormBuilder     = new TableFormBuilder();
        $bootstrapFormBuilder = new BootstrapFormBuilder();
        $flexBoxFormBuilder   = new FlexBoxFormBuilder();


        $feedBackBootstrapForm = new FeedBackFormDirector();
        $feedBackBootstrapForm->build($tableFormBuilder);

        $feedBackBootstrapForm = new FeedBackFormDirector();
        $feedBackBootstrapForm->build($bootstrapFormBuilder);

        $feedBackBootstrapForm = new FeedBackFormDirector();
        $feedBackBootstrapForm->build($flexBoxFormBuilder);

        $this->showForm($tableFormBuilder);
        $this->showForm($bootstrapFormBuilder);
        $this->showForm($flexBoxFormBuilder);

        $reviewBootstrapForm = new ReviewFormDirector();
        $reviewBootstrapForm->build($tableFormBuilder);

        $reviewBootstrapForm = new ReviewFormDirector();
        $reviewBootstrapForm->build($bootstrapFormBuilder);

        $reviewBootstrapForm = new ReviewFormDirector();
        $reviewBootstrapForm->build($flexBoxFormBuilder);


        $this->showForm($tableFormBuilder);
        $this->showForm($bootstrapFormBuilder);
        $this->showForm($flexBoxFormBuilder);
    }

    /**
     * @param IFormBuilder $formBuilder
     * @throws Exception
     */
    private function showForm(IFormBuilder $formBuilder): void
    {
        Report::log($formBuilder->make());
    }
}
