<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service;

/**
 * Interface ILogger.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service
 */
interface ILogger
{
    /**
     * @param string $channel
     */
    public function setChannel(string $channel): void;

    /**
     * @param string $context
     * @param array  $args
     */
    public function log(string $context,array $args = []): void;
}
