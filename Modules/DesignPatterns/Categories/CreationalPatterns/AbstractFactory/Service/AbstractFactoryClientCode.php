<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter\EventNewsletter;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\BeautifulNewsNewsletter;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\NewsNewsletter;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter\SertificateNewsletter;
use Modules\DesignPatterns\Service\IClientCode;

/**
 * Class AbstractFactoryClientCode.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service
 */
class AbstractFactoryClientCode implements IClientCode
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'abstract-factory';
    }

    /**
     * @inheritDoc
     */
    public function main(): void
    {
        $eventNewsLetter         = new EventNewsletter();
        $newsNewsletter          = new NewsNewsletter();
        $beautifulNewsNewsletter = new BeautifulNewsNewsletter();
        $sertificateNewsLetter   = new SertificateNewsletter();

        $this->sendNewsLetter($eventNewsLetter);
        $this->sendNewsLetter($newsNewsletter);
        $this->sendNewsLetter($beautifulNewsNewsletter);
        $this->sendNewsLetter($sertificateNewsLetter);
    }

    /**
     * @param INewsletter $newsletter
     */
    private function sendNewsLetter(INewsletter $newsletter): void
    {
        $configurator = $newsletter->getConfigurator();
        $scheduler    = $newsletter->getScheduler();

        $clientCode = $this;
        $scheduler->task($configurator, function () use ($newsletter, $clientCode) {
            $logger = $newsletter->getLogger();

            $logger->setChannel($clientCode->getClassName($newsletter));
            $logger->log('Start Newsletter..');

            $logger->log("Init DataManager ..");
            $dataManager = $newsletter->getDataManager();
            $logger->log("DataManager {$clientCode->getClassName($dataManager)} get data..");
            $data = $dataManager->getData();
            $logger->log("DataManager {$clientCode->getClassName($dataManager)} get data is ready", $data);

            $logger->log("Init Template ..");
            $template = $newsletter->getTemplate();
            $logger->log("Init Template {$clientCode->getClassName($template)} is ready");

            $logger->log("Init Sender ..");
            $sender = $newsletter->getSender();
            $logger->log("Init Sender {$clientCode->getClassName($sender)} is ready");

            foreach ($data as $dataItem) {
                $userId = $dataItem['userId'];

                $logger->log("Template {$clientCode->getClassName($template)} makeResult..");
                $makeResult = $template->makeResult($dataItem);
                $logger->log("Template {$clientCode->getClassName($template)} makeResult is ready", [$makeResult]);

                $logger->log("Sender {$clientCode->getClassName($sender)} send for userId {$userId}..");
                $sender->send($userId, $makeResult);
                $logger->log("Sender {$clientCode->getClassName($sender)} send for userId {$userId} is ready");
            }

            $logger->log('End Newsletter');
        });
    }

    /**
     * @param $object
     * @return string
     */
    public function getClassName($object): string
    {
        $pathClassArray = explode('\\', get_class($object));

        return last($pathClassArray);
    }
}
