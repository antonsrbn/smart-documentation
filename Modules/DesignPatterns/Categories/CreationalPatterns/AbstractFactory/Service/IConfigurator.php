<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service;

/**
 * Interface IConfigurator.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service
 */
interface IConfigurator
{
    /**
     * @return string
     */
    public function getPeriod(): string;

    /**
     * @return int
     */
    public function getDelay(): int;
}
