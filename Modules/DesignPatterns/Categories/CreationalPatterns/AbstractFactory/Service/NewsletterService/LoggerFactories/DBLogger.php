<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\LoggerFactories;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ILogger;
use Modules\DesignPatterns\Service\Report;

/**
 * Class DBLogger.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\LoggerFactories
 */
class DBLogger implements ILogger
{
    private string $tableName;
    private string $channel;

    /**
     * @inheritDoc
     */
    public function setChannel(string $channel): void
    {
        $this->channel = $channel;
    }

    /**
     * DBLogger constructor.
     *
     * @param string $tableName
     */
    public function __construct(string $tableName)
    {
        $this->tableName = $tableName;
        $this->channel   = 'default';
    }

    /**
     * @inheritDoc
     */
    public function log(string $context, array $args = []): void
    {
        $args = json_encode($args);
        Report::log("DBLogger: table: {$this->tableName} channel: {$this->channel}
        context: {$context} args: {$args}");
    }
}
