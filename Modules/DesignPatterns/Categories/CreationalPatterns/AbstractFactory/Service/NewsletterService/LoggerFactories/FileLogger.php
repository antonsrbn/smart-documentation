<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\LoggerFactories;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ILogger;
use Modules\DesignPatterns\Service\Report;

/**
 * Class FileLogger.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\LoggerFactories
 */
class FileLogger implements ILogger
{
    private string $directory;
    private string $channel;


    /**
     * @inheritDoc
     */
    public function setChannel(string $channel): void
    {
        $this->channel = $channel;
    }
    /**
     * FileLogger constructor.
     */
    public function __construct()
    {
        $this->directory = 'default';
        $this->channel   = 'default';
    }

    /**
     * @param string $directory
     */
    public function setDirectory(string $directory): void
    {
        $this->directory = $directory;
    }

    /**
     * @inheritDoc
     */
    public function log(string $context, array $args = []): void
    {
        $args = json_encode($args);
        Report::log("FileLogger: directory: {$this->directory} channel: {$this->channel}
        context: {$context} args: {$args}");
    }
}
