<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\ScheduleFactories;


use Closure;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IConfigurator;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ISchedule;
use Modules\DesignPatterns\Service\Report;

/**
 * Class LaravelScheduler.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\ScheduleFactories
 */
class LaravelScheduler implements ISchedule
{
    /**
     * @inheritDoc
     */
    public function task(IConfigurator $configurator, Closure $task): void
    {
        Report::log("LaravelScheduler: run task with period {$configurator->getPeriod()} and
        width delay {$configurator->getDelay()} seconds");

        $task->call($this);
    }
}
