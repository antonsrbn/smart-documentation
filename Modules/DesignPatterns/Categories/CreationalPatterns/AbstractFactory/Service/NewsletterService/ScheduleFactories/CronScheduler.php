<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\ScheduleFactories;


use Closure;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IConfigurator;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ISchedule;
use Modules\DesignPatterns\Service\Report;

/**
 * Class CronScheduler.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\ScheduleFactories
 */
class CronScheduler implements ISchedule
{
    /**
     * @inheritDoc
     */
    public function task(IConfigurator $configurator, Closure $task): void
    {
        Report::log("CronScheduler: run task with period {$configurator->getPeriod()}");

        $task->call($this);
    }
}
