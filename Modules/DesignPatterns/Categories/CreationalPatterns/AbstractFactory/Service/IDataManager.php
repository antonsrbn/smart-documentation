<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service;

/**
 * Interface IDataManager.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service
 */
interface IDataManager
{
    /**
     * @return array
     */
    public function getData(): array;

}
