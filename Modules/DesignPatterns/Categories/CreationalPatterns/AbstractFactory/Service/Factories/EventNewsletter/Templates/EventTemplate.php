<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter\Templates;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ITemplate;

/***
 * Class EventTemplate.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter\Templates
 */
class EventTemplate implements ITemplate
{
    /**
     * @inheritDoc
     */
    public function makeResult(array $data): string
    {
      return "Hello {$data['name']}, your order № {$data['orderId']} has arrived at the pick-up point!";
    }
}
