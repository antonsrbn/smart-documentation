<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter\Configurations\MorningConfigurator;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter\DataManagers\OrderSuccessDataManager;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter\Templates\EventTemplate;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IConfigurator;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IDataManager;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ILogger;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\INewsletter;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ISchedule;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ITemplate;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\LoggerFactories\FileLogger;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\ScheduleFactories\CronScheduler;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\SmsSender\SmsSenderCreator;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISender;

/**
 * Class EventNewsletter.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter
 */
class EventNewsletter implements INewsletter
{
    /**
     * @inheritDoc
     */
    public function getConfigurator(): IConfigurator
    {
        return new MorningConfigurator();
    }

    /**
     * @inheritDoc
     */
    public function getDataManager(): IDataManager
    {
        return new OrderSuccessDataManager();
    }

    /**
     * @inheritDoc
     */
    public function getTemplate(): ITemplate
    {
        return new EventTemplate();
    }

    /**
     * @inheritDoc
     */
    public function getLogger(): ILogger
    {
        $fileLogger = new FileLogger();
        $fileLogger->setDirectory('events');

        return $fileLogger;
    }

    /**
     * @inheritDoc
     */
    public function getSender(): ISender
    {
        return (new SmsSenderCreator())
            ->createSender();
    }

    /**
     * @inheritDoc
     */
    public function getScheduler(): ISchedule
    {
        return new CronScheduler();
    }
}
