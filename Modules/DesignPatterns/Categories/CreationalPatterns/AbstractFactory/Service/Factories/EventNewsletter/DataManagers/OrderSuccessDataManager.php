<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter\DataManagers;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IDataManager;

/**
 * Class NewProductDataManager.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter\DataManagers
 */
class OrderSuccessDataManager implements IDataManager
{
    /**
     * @inheritDoc
     */
    public function getData(): array
    {
        return [
            [
                'userId'  => 123,
                'name'    => 'Jhon',
                'orderId' => 653,
            ],
        ];
    }
}
