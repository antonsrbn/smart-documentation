<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter\Configurations;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IConfigurator;

/**
 * Class MorningConfigurator.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\EventNewsletter\Configurations
 */
class MorningConfigurator implements IConfigurator
{
    /**
     * @inheritDoc
     */
    public function getPeriod(): string
    {
        return 'Morning: 8:00';
    }

    /**
     * @inheritDoc
     */
    public function getDelay(): int
    {
        return 0;
    }
}
