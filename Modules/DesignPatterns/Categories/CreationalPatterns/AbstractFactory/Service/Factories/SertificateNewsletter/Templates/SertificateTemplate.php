<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter\Templates;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ITemplate;

/**
 * Class SertificateTemplate.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter\Templates
 */
class SertificateTemplate implements ITemplate
{
    /**
     * @inheritDoc
     */
    public function makeResult(array $data): string
    {
        return "Hello {$data['name']}, you are great and you have coped with this task, go to your personal account to get a certificate!";
    }
}
