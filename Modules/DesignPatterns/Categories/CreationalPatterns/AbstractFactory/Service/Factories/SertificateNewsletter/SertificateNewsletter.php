<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter\Configurations\SertificateConfigurator;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter\DataManagers\SertificateDataManager;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter\Templates\SertificateTemplate;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IConfigurator;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IDataManager;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ILogger;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\INewsletter;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ISchedule;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ITemplate;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\LoggerFactories\DBLogger;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\ScheduleFactories\LaravelScheduler;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\UniSender\UniSenderCreator;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISender;

/**
 * Class SertificateNewsletter.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter
 */
class SertificateNewsletter implements INewsletter
{
    /**
     * @inheritDoc
     */
    public function getConfigurator(): IConfigurator
    {
        return new SertificateConfigurator();
    }

    /**
     * @inheritDoc
     */
    public function getDataManager(): IDataManager
    {
        return new SertificateDataManager();
    }

    /**
     * @inheritDoc
     */
    public function getTemplate(): ITemplate
    {
        return new SertificateTemplate();
    }

    /**
     * @inheritDoc
     */
    public function getLogger(): ILogger
    {
        return new DBLogger('sertificate_newsletter');
    }

    /**
     * @inheritDoc
     */
    public function getSender(): ISender
    {
        return (new UniSenderCreator())
            ->createSender();
    }

    /**
     * @inheritDoc
     */
    public function getScheduler(): ISchedule
    {
        return new LaravelScheduler();
    }
}
