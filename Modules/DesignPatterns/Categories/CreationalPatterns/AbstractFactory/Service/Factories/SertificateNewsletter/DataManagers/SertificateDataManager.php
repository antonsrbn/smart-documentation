<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter\DataManagers;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IDataManager;

/**
 * Class SertificateDataManager.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter\DataManagers
 */
class SertificateDataManager implements IDataManager
{
    /**
     * @inheritDoc
     */
    public function getData(): array
    {
        return [
            [
                'userId' => 123,
                'name'   => 'Jhon',
            ],
            [
                'userId' => 124,
                'name'   => 'Lesson',
            ],
            [
                'userId' => 125,
                'name'   => 'Pol',
            ],
        ];
    }
}
