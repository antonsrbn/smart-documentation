<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter\Configurations;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IConfigurator;

/**
 * Class SertificateNewsletter.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\SertificateNewsletter\Configurations
 */
class SertificateConfigurator implements IConfigurator
{
    /**
     * @inheritDoc
     */
    public function getPeriod(): string
    {
        return 'Sertificate: Now and immediately';
    }

    /**
     * @inheritDoc
     */
    public function getDelay(): int
    {
        return 30;
    }
}
