<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\DataManagers;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IDataManager;

/**
 * Class NewNewsDataManager.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\DataManagers
 */
class NewNewsDataManager implements IDataManager
{
    /**
     * @inheritDoc
     */
    public function getData(): array
    {
        return [
            [
                'userId'   => 123,
                'newsName' => 'Hello word is worked!',
            ],
            [
                'userId'   => 124,
                'newsName' => 'Hello word is worked!',
            ],
            [
                'userId'   => 125,
                'newsName' => 'Hello word is worked!',
            ],
            [
                'userId'   => 125,
                'newsName' => 'Abstract factory is worked!',
            ],
        ];
    }
}
