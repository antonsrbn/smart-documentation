<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\Configurations\FridayConfigurator;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\DataManagers\NewNewsDataManager;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\Templates\BeautifulTemplate;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IConfigurator;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IDataManager;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ILogger;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\INewsletter;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ISchedule;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ITemplate;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\LoggerFactories\FileLogger;
use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\NewsletterService\ScheduleFactories\CronScheduler;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\UniSender\UniSenderCreator;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISender;

/**
 * Class BeautifulNewsNewsletter.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter
 */
class BeautifulNewsNewsletter implements INewsletter
{
    /**
     * @inheritDoc
     */
    public function getConfigurator(): IConfigurator
    {
        return new FridayConfigurator();
    }

    /**
     * @inheritDoc
     */
    public function getDataManager(): IDataManager
    {
        return new NewNewsDataManager();
    }

    /**
     * @inheritDoc
     */
    public function getTemplate(): ITemplate
    {
        return new BeautifulTemplate();
    }

    /**
     * @inheritDoc
     */
    public function getLogger(): ILogger
    {
        $fileLogger = new FileLogger();
        $fileLogger->setDirectory('events');

        return $fileLogger;
    }

    /**
     * @inheritDoc
     */
    public function getSender(): ISender
    {
        return (new UniSenderCreator())
            ->createSender();
    }

    /**
     * @inheritDoc
     */
    public function getScheduler(): ISchedule
    {
        return new CronScheduler();
    }
}
