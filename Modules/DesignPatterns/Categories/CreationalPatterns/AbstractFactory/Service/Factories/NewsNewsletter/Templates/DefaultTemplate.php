<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\Templates;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ITemplate;

/**
 * Class DefaultTemplate.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\Templates
 */
class DefaultTemplate implements ITemplate
{
    /**
     * @inheritDoc
     */
    public function makeResult(array $data): string
    {
        return "New news: `{$data['newsName']}` ..";
    }
}
