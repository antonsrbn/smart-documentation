<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\Configurations;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\IConfigurator;

/**
 * Class FridayConfigurator.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\Configurations
 */
class FridayConfigurator implements IConfigurator
{
    /**
     * @inheritDoc
     */
    public function getPeriod(): string
    {
        return 'Friday: 17:00';
    }

    /**
     * @inheritDoc
     */
    public function getDelay(): int
    {
        return 0;
    }
}
