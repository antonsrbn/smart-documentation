<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\Templates;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\ITemplate;

/**
 * Class BeautifulTemplate.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\Factories\NewsNewsletter\Templates
 */
class BeautifulTemplate implements ITemplate
{
    /**
     * @inheritDoc
     */
    public function makeResult(array $data): string
    {
        return "You will like the news about `{$data['newsName']}`.";
    }
}
