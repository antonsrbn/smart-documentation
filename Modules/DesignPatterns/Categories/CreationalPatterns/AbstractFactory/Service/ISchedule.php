<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service;

use Closure;

/**
 * Interface ISchedule.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service
 */
interface ISchedule
{
    /**
     * @param IConfigurator $configurator
     * @param Closure       $task
     * @return void
     */
    public function task(IConfigurator $configurator, Closure $task): void;
}
