<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service;

/**
 * Interface ITemplate.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service
 */
interface ITemplate
{
    /**
     * @param array $data
     * @return string
     */
    public function makeResult(array $data): string;
}
