<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service;


use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISender;

/**
 * Interface INewsletter.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service
 */
interface INewsletter
{
    /**
     * @return IConfigurator
     */
    public function getConfigurator(): IConfigurator;

    /**
     * @return IDataManager
     */
    public function getDataManager(): IDataManager;

    /**
     * @return ITemplate
     */
    public function getTemplate(): ITemplate;

    /**
     * @return ILogger
     */
    public function getLogger(): ILogger;

    /***
     * @return ISender
     */
    public function getSender(): ISender;

    /**
     * @return ISchedule
     */
    public function getScheduler(): ISchedule;
}
