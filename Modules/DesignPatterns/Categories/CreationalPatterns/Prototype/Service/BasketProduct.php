<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Prototype\Service;

use Exception;

/**
 * Class BasketProduct.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Prototype\Service
 */
class BasketProduct
{
    private int    $id;
    private string $name;
    private int    $quantity;
    private static array $basketProductsDb = [];

    /**
     * Products constructor.
     *
     * @param int    $id
     * @param string $name
     * @param int    $quantity
     * @param int    $quantityInStore
     */
    public function __construct(int $id, string $name, int $quantity, int $quantityInStore)
    {
        $this->id       = $id;
        $this->name     = $name;
        $this->quantity = $quantity;

        self::$basketProductsDb[$id] = [
            'quantityInStore' => $quantityInStore,
        ];
    }

    public function getQuantityInStore(): int
    {
        return self::$basketProductsDb[$this->id]['quantityInStore'];
    }

    /**
     * @throws Exception
     */
    public function __clone()
    {
        $quantityInStore = self::$basketProductsDb[$this->id]['quantityInStore'] - $this->quantity;

        if ($quantityInStore < 0) {
            throw new Exception("Product {$this->id} not clone, after in quantityInStore < 0");
        }

        self::$basketProductsDb[$this->id]['quantityInStore'] = $quantityInStore;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
