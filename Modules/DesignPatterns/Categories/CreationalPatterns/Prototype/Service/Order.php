<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Prototype\Service;


use DateTime;
use Exception;

/**
 * Class Order.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Prototype\Service
 */
class Order
{
    private int      $id;
    private DateTime $dateCreate;
    private array    $basketProducts;

    /**
     * Order constructor.
     *
     * @param int      $id
     * @param DateTime $dateCreate
     * @param array    $basketProducts
     */
    public function __construct(int $id, DateTime $dateCreate, array $basketProducts)
    {
        $this->id             = $id;
        $this->dateCreate     = $dateCreate;
        $this->basketProducts = $basketProducts;
    }

    /**
     * @throws Exception
     */
    public function __clone()
    {
        $this->dateCreate = new DateTime();
        $basketProducts   = [];

        foreach ($this->basketProducts as $basketProduct) {
            try {
                $basketProducts[] = clone $basketProduct;
            } catch (Exception $exception) {
                throw new Exception("Before order id {$this->id} clone: " . $exception->getMessage());
            }
        }

        $this->basketProducts = $basketProducts;
        $this->id++;
    }

    /**
     * @return array
     */
    public function getInfo(): array
    {
        $basketProducts = [];

        foreach ($this->basketProducts as $basketProduct) {
            /**
             * @var $basketProduct BasketProduct
             */
            $basketProducts[] = [
                'id'              => $basketProduct->getId(),
                'name'            => $basketProduct->getName(),
                'quantity'        => $basketProduct->getQuantity(),
                'quantityInStore' => $basketProduct->getQuantityInStore(),
            ];
        }

        return [
            'id'             => $this->id,
            'dateCreate'     => $this->dateCreate->format('Y.m.d H:i:s'),
            'basketProducts' => $basketProducts,
        ];
    }
}
