<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Prototype\Service;


use DateTime;
use Exception;
use Modules\DesignPatterns\Service\IClientCode;
use Modules\DesignPatterns\Service\Report;

/**
 * Class PrototypeClientCode.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Prototype\Service
 */
class PrototypeClientCode implements IClientCode
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'prototype';
    }

    /**
     * @inheritDoc
     */
    public function main(): void
    {
        $orderId = 56;
        $order1  = $this->loadOrderById($orderId);


        try {
            $this->showResult($order1);


            $order2 = clone $order1;
            $this->showResult($order2);


            $order3 = clone $order1;
            $this->showResult($order3);


            $order4 = clone $order1;
            $this->showResult($order4);
        } catch (Exception $e) {
            Report::log('Error: ' . $e->getMessage());
        }

    }

    /**
     * @param Order $order
     * @throws Exception
     */
    private function showResult(Order $order): void
    {
        $orderInfo = $order->getInfo();
        $products  = '';

        foreach ($orderInfo['basketProducts'] as $basketProduct) {
            $products .= "\n\tProduct" .
                " id:{$basketProduct['id']}" .
                " name: {$basketProduct['name']}" .
                " quantity: {$basketProduct['quantity']}" .
                " quantityInStore: {$basketProduct['quantityInStore']}";
        }

        Report::log("Order id: {$orderInfo['id']} date create: {$orderInfo['dateCreate']} $products");
    }

    /**
     * @param int $orderId
     * @return Order
     */
    private function loadOrderById(int $orderId): Order
    {
        $basketProducts = [
            new BasketProduct(135, 'Пила для дерева', 2, 6),
            new BasketProduct(136, 'Шуруповерт', 1, 2),
            new BasketProduct(137, 'Бензопила', 4, 15),
        ];

        $datTime = (new DateTime())
            ->setDate(2021, 1, 2)
            ->setTime(11, 34, 03);

        return new Order($orderId, $datTime, $basketProducts);
    }
}
