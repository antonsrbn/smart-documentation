# Прототип

## Смысл

Копирование объекта, не вдаваясь в подробную их реализацию.

## Когда следует применить

+ Когда нужна независимость к реализации конкретного объекта при его клонировании;
+ Когда есть подклассы, измененными значениями полей от родительского, в него и определяется прототип.

## Преимущества

+ Копирование продукта без привязок к конкретному продукту;
+ Уменьшение кода с инициализацией объектов;
+ Ускоренное порождение продукта;
+ Удобное создание подкласса при конструировании сложного продукта.

## Недостатки

+ Может произойти неполное копирование, когда объект имеет внутри себя ссылки на другие объекты.

## Пример

### Создание нового заказа с проверкой

Когда нужно скопировать заказ, но нужно учесть все проверки.

Команда запуска результата: `php artisan client-code:run --category=creational --clientCode=prototype`

