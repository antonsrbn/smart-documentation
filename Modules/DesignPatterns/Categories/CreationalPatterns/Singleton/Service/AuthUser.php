<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Singleton\Service;

use Exception;

/**
 * Class AuthUser.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Singleton\Service
 */
final class AuthUser
{
    /**
     * @var AuthUser
     */
    private static $instance;
    private int    $userId;

    /**
     * @return self
     */
    public static function get(): self
    {
        if (self::$instance === null) {
            self::$instance = new self(1);
        }

        return self::$instance;
    }

    /**
     * @param int $userId
     * @return static
     */
    public function authorizeById(int $userId): self
    {
        $this->userId = $userId;

        return self::$instance;
    }

    /**
     * AuthUser constructor.
     *
     * @param int $userId
     */
    private function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * Одиночки не должны быть клонируемыми.
     *
     * @throws Exception
     */
    protected function __clone()
    {
        throw new Exception('Cannot clone a singleton');
    }

    /**
     * Одиночки не должны быть восстанавливаемыми из строк.
     *
     * @throws Exception
     */
    public function __wakeup()
    {
        throw new Exception('Cannot unserialize a singleton');
    }

}
