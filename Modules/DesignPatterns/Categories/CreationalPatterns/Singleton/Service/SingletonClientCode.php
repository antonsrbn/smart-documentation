<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\Singleton\Service;


use Modules\DesignPatterns\Service\IClientCode;
use Modules\DesignPatterns\Service\Report;

/**
 * Class SingletonClientCode.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\Singleton\Service
 */
class SingletonClientCode implements IClientCode
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'singleton';
    }

    /**
     * @inheritDoc
     */
    public function main(): void
    {
        $authUser1 = AuthUser::get();
        Report::log("Auth User1 id: {$authUser1->getUserId()}");

        Report::log("Auth User2 from method instance ..");

        $authUser2 = AuthUser::get()->authorizeById(42);

        Report::log("Auth User2 id: {$authUser2->getUserId()}");

        Report::log("Auth User3 from method object Auth User2 ..");

        $authUser3 = $authUser2->authorizeById(76);

        Report::log("Auth User3 id: {$authUser3->getUserId()}");


        Report::log("Auth User1 id: {$authUser1->getUserId()}");
        Report::log("Auth User2 id: {$authUser2->getUserId()}");
        Report::log("Auth User3 id: {$authUser3->getUserId()}");
    }
}
