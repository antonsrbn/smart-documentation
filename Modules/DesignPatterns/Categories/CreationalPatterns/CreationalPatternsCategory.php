<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns;


use Modules\DesignPatterns\Categories\CreationalPatterns\AbstractFactory\Service\AbstractFactoryClientCode;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\BuilderClientCode;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\FactoryMethodClientCode;
use Modules\DesignPatterns\Categories\CreationalPatterns\Prototype\Service\PrototypeClientCode;
use Modules\DesignPatterns\Categories\CreationalPatterns\Singleton\Service\SingletonClientCode;
use Modules\DesignPatterns\Service\ICategory;

/**
 * Class CreationalPatternsCategory.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns
 */
class CreationalPatternsCategory implements ICategory
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'creational';
    }

    /**
     * @inheritDoc
     */
    public function getClientCodes(): array
    {
        return [
            FactoryMethodClientCode::class,
            AbstractFactoryClientCode::class,
            BuilderClientCode::class,
            PrototypeClientCode::class,
            SingletonClientCode::class,
        ];
    }

}
