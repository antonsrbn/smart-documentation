<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service;

/**
 * Interface ISenderCreator.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service
 */
interface ISenderCreator
{
    /**
     * @return ISender
     */
    public function createSender(): ISender;
}
