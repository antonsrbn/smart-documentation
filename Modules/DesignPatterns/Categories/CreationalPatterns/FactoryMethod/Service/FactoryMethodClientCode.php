<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service;


use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\MailSender\MailSenderCreator;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\SmsSender\SmsSenderCreator;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\UniSender\UniSenderCreator;
use Modules\DesignPatterns\Service\IClientCode;

class FactoryMethodClientCode implements IClientCode
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'factory-method';
    }

    /**
     * @inheritDoc
     */
    public function main(): void
    {
        $mailSenderCreator = new MailSenderCreator();
        $smsSenderCreator  = new SmsSenderCreator();
        $uniSenderCreator  = new UniSenderCreator();

        $this->send($mailSenderCreator);
        $this->send($smsSenderCreator);
        $this->send($uniSenderCreator);
    }

    private function send(ISenderCreator $senderCreator): void
    {
        $senderCreator
            ->createSender()
            ->send('123', '"test factory method!"');
    }

}
