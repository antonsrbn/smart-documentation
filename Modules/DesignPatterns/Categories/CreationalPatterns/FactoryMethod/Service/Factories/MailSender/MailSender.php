<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\MailSender;

use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISender;
use Modules\DesignPatterns\Service\Report;

/**
 * Class MailSender
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\MailSender
 */
class MailSender implements ISender
{
    private string $from;

    /**
     * @inheritDoc
     */
    public function send(string $userId, string $text): void
    {
        Report::log("Native mail send to user {$userId} text {$text} from {$this->from}");
    }

    public function setFromEmail(string $email)
    {
        $this->from = $email;
    }
}
