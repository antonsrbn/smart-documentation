<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\MailSender;


use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISender;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISenderCreator;

/**
 * Class MailSenderCreator.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\MailSender
 */
class MailSenderCreator implements ISenderCreator
{

    /**
     * @inheritDoc
     */
    public function createSender(): ISender
    {
        $mailSender = new MailSender();

        $mailSender->setFromEmail('root@root.ru');

        return $mailSender;
    }
}
