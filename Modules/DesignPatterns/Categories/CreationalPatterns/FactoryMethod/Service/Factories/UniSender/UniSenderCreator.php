<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\UniSender;


use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISender;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISenderCreator;

/**
 * Class UniSenderCreator
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\SmsSender
 */
class UniSenderCreator implements ISenderCreator
{
    const LOGIN = 'uni-operator';
    const PASSWORD = 'uni-password';
    const SERVER_NAME = 's1';
    /**
     * @inheritDoc
     */
    public function createSender(): ISender
    {
        $uniSender = new UniSender(self::LOGIN,self::PASSWORD);

        $uniSender->setServerName(self::SERVER_NAME);

        return $uniSender;
    }
}
