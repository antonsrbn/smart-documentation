<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\UniSender;


use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISender;
use Modules\DesignPatterns\Service\Report;

/**
 * Class UniSender
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\SmsSender
 */
class UniSender implements ISender
{
    private string $login;
    private string $password;
    private string $serverName;

    /**
     * SmsSender constructor.
     *
     * @param string $login
     * @param string $password
     */
    public function __construct(string $login, string $password)
    {
        $this->login      = $login;
        $this->password   = $password;
        $this->serverName = 'default';
    }

    /**
     * @inheritDoc
     */
    public function send(string $userId, string $text): void
    {
        Report::log("Unisender mail send to user
        {$userId} text {$text} from
        this login {$this->login} as password {$this->password} on server {$this->serverName}");
    }

    /**
     * @param string $serverName
     */
    public function setServerName(string $serverName): void
    {
        $this->serverName = $serverName;
    }
}
