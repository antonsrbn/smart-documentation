<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\SmsSender;


use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISender;
use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISenderCreator;

/**
 * Class SmsSenderCreator
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\SmsSender
 */
class SmsSenderCreator implements ISenderCreator
{
    const LOGIN = 'sms-operator';
    const PASSWORD = 'sms-password';
    /**
     * @inheritDoc
     */
    public function createSender(): ISender
    {
        return new SmsSender(self::LOGIN,self::PASSWORD);
    }
}
