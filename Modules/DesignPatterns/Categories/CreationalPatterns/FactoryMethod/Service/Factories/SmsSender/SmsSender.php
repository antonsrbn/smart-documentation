<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\SmsSender;


use Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\ISender;
use Modules\DesignPatterns\Service\Report;

/**
 * Class SmsSender
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service\Factories\SmsSender
 */
class SmsSender implements ISender
{
    private string $login;
    private string $password;

    /**
     * SmsSender constructor.
     *
     * @param string $login
     * @param string $password
     */
    public function __construct(string $login, string $password)
    {
        $this->login    = $login;
        $this->password = $password;
    }

    /**
     * @inheritDoc
     */
    public function send(string $userId, string $text): void
    {
        Report::log("Sms mail send to user {$userId} text {$text} from
        this login {$this->login} as password {$this->password}");
    }
}
