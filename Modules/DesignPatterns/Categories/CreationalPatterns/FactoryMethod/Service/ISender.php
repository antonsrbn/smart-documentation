<?php


namespace Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service;

/**
 * Interface ISender.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns\FactoryMethod\Service
 */
interface ISender
{
    /**
     * @param string $userId
     * @param string $text
     */
    public function send(string $userId, string $text): void;
}
