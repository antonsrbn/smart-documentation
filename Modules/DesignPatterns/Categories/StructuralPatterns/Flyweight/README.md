# Легковес

## Смысл

Основная цель — экономия памяти, вынос общих повторяющихся данных, в группу типов.

## Когда следует применить

+ Когда не хватает оперативной памяти для поддержки всех нужных объектов;
+ По той же причине, что и выше, но парным применением к
  [компоновщику](../Composite/README.md).

## Преимущества

+ Экономит оперативную память.

## Недостатки

+ Нагружает работу процессора, т.к. приходится по справочнику выискивать легковес;
+ Код программы становится усложненный, т.к. вводится множество дополнительных классов.

## Пример

### Древовидная структура XML документа

В группу легковеса вынесены общий набор разметок.

Команда запуска результата: `php artisan client-code:run --category=structural --clientCode=flyweight`

