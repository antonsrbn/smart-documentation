<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns;


use Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\AdapterClientCode;
use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\BridgeClientCode;
use Modules\DesignPatterns\Service\ICategory;

/**
 * Class StructuralPatterns.
 *
 * @package Modules\DesignPatterns\Categories\CreationalPatterns
 */
class StructuralPatterns implements ICategory
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'structural';
    }

    /**
     * @inheritDoc
     */
    public function getClientCodes(): array
    {
        return [
            AdapterClientCode::class,
            BridgeClientCode::class,
        ];
    }

}
