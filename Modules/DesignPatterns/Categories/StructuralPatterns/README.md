# Структурные паттерны

## Смысл

Эти паттерны отвечают за построение удобных в поддержке иерархий классов.

## Ссылки

+ [Адаптер](Adapter/README.md);
+ [Мост](Bridge/README.md);
+ [Компоновщик](Composite/README.md);
+ [Декоратор](Decorator/README.md);
+ [Фасад](Facade/README.md);
+ [Легковес](Flyweight/README.md);
+ [Заместитель](Proxy/README.md).
