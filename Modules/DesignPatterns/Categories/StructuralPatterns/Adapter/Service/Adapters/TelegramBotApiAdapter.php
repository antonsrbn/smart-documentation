<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\Adapters;

use Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\ILogger;
use Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices\TelegramBotApi;

/**
 * Class TelegramBotApiAdapter.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\Adapters
 */
class TelegramBotApiAdapter implements ILogger
{
    private TelegramBotApi $telegramBotApi;

    private int $channel;

    /**
     * TelegramBotApiAdapter constructor.
     *
     * @param TelegramBotApi $telegramBotApi
     * @param int            $channel
     */
    public function __construct(TelegramBotApi $telegramBotApi, int $channel)
    {
        $this->telegramBotApi = $telegramBotApi;
        $this->channel        = $channel;
    }

    /**
     * @inheritDoc
     */
    public function log(string $log): void
    {
        $this->telegramBotApi->sendMessage($this->channel, $log);
    }
}
