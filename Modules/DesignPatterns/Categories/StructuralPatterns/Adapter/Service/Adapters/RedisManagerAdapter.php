<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\Adapters;


use Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices\FileLogger;
use Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices\RedisManager;

/**
 * Class RedisManagerAdapter.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\Adapters
 */
class RedisManagerAdapter extends FileLogger
{
    /**
     * @var RedisManager
     */
    private RedisManager $redisManager;

    /**
     * @param RedisManager $redisManager
     */
    public function initService(RedisManager $redisManager): void
    {
        $this->redisManager = $redisManager;
    }

    /**
     * @param string $text
     */
    public function writeToFile(string $text): void
    {
        $this->redisManager->updateRow($text);
    }

}
