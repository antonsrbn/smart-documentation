<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices;

use Modules\DesignPatterns\Service\Report;

/**
 * Class FileLogger.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices
 */
class FileLogger
{
    /**
     * @param string $text
     */
    public function writeToFile(string $text): void
    {
        Report::log("writeToFile text: {$text}");
    }
}
