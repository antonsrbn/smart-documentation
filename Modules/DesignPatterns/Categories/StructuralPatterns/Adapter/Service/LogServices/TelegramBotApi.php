<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices;

use Modules\DesignPatterns\Service\Report;

/**
 * Class TelegramBotApi.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices
 */
class TelegramBotApi
{
    /**
     * @param int    $channel
     * @param string $message
     */
    public function sendMessage(int $channel, string $message): void
    {
        Report::log("Send message for on channel: {$channel} message: {$message}");
    }
}
