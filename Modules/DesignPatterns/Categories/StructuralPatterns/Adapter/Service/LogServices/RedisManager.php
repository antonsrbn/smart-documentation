<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices;

use Modules\DesignPatterns\Service\Report;

/**
 * Class RedisManager.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices
 */
class RedisManager
{
    /**
     * @param string      $row
     * @param string|null $key
     * @return string
     */
    public function updateRow(string $row, ?string $key = null): string
    {
        Report::log(($key !== null ? 'Insert/Update' : 'Insert new') . " row in Redis row: {$row}");
        // new key
        return md5('53534673');
    }
}
