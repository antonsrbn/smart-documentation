<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service;


use Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\Adapters\RedisManagerAdapter;
use Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\Adapters\TelegramBotApiAdapter;
use Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices\FileLogger;
use Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices\RedisManager;
use Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service\LogServices\TelegramBotApi;
use Modules\DesignPatterns\Service\IClientCode;

/**
 * Class AdapterClientCode.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service
 */
class AdapterClientCode implements IClientCode
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'adapter';
    }

    /**
     * @inheritDoc
     */
    public function main(): void
    {
        $fileLogger = new FileLogger();
        $this->subClientCode1($fileLogger);

        $redisManager        = new RedisManager();
        $redisManagerAdapter = new RedisManagerAdapter();
        $redisManagerAdapter->initService($redisManager);

        $this->subClientCode1($redisManagerAdapter);

        $telegramBotApi        = new TelegramBotApi();
        $telegramBotApiAdapter = new TelegramBotApiAdapter($telegramBotApi, 45625);

        $this->subClientCode2($telegramBotApiAdapter);
    }

    /**
     * @param FileLogger $fileLogger
     */
    private function subClientCode1(FileLogger $fileLogger): void
    {
        $fileLogger->writeToFile('create new api log.');
    }

    /**
     * @param ILogger $logger
     */
    private function subClientCode2(ILogger $logger): void
    {
        $logger->log('create new system log.');
    }
}
