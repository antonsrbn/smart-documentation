<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service;

/**
 * Interface ILogger.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Adapter\Service
 */
interface ILogger
{
    /**
     * @param string $log
     */
    public function log(string $log): void;
}
