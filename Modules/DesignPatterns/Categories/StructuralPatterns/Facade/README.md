# Фасад

## Смысл

Простой интерфейс работы со сложными подсистемами.

## Когда следует применить

+ Когда вам нужно представить простой или урезанный интерфейс к сложной подсистеме;
+ Когда сервисы должны общаться по простым правилам.

## Преимущества

+ Скрывает сложную реализацию управления подсистемой.

## Недостатки

+ Может сильно вырасти под капотом, привязываясь к правилам подсистемы.

## Пример

### Библиотека конвертации изображения в webp

Используется головной класс, которой под капотом использует куча подсервисов для рендеринга webp файла.

Команда запуска результата: `php artisan client-code:run --category=structural --clientCode=facade`
