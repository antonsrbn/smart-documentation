<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\FormBuilders;


use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\BaseFormBuilder;
use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\IWebBrowserEngine;

/**
 * Class ReactFormBuilder.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\FormBuilders
 */
class ReactFormBuilder extends BaseFormBuilder
{
    /**
     * ReactFormBuilder constructor.
     *
     * @param IWebBrowserEngine $webBrowserEngine
     */
    public function __construct(IWebBrowserEngine $webBrowserEngine)
    {
        parent::__construct($webBrowserEngine);

        $this->webBrowserEngine->loadRemoteFilesFromCache();
        $this->webBrowserEngine->loadRemoteFiles();
        $this->webBrowserEngine->disableDisplay();
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title): self
    {
        $title = '<ReactHeader>' . $title . '</ReactHeader>';

        [$height, $width] = $this->calcElementPosition();
        $this->webBrowserEngine->printText($title, $height, $width, 12);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addTextField(string $name, string $id, bool $isRequired = false): self
    {
        $text = "<ReactTextField {$this->makeRequired($isRequired)} name ='{$id}'>" . $name . '</ReactTextField>';

        [$height, $width] = $this->calcElementPosition();
        $this->webBrowserEngine->printText($text, $height, $width, 12);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addCheckboxField(string $name, string $id, bool $isRequired = false): self
    {
        $checkBoxText = "<ReactCheckBoxFieldText {$this->makeRequired($isRequired)} name ='{$id}'>" . $name . '</ReactCheckBoxFieldText>';
        $checkBox     = "<ReactCheckBoxField {$this->makeRequired($isRequired)} name ='{$id}'>" . $name . '</ReactCheckBoxField>';

        [$height, $width] = $this->calcElementPosition();
        $this->webBrowserEngine->printText($checkBoxText, $height, $width, 12);
        $this->webBrowserEngine->printInput($checkBox, $height, $width);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addTexAreaField(string $name, string $id, bool $isRequired = false): self
    {
        $name = "<ReactTextAreaField {$this->makeRequired($isRequired)} name ='{$id}'>" . $name . '</ReactTextAreaField>';

        [$height, $width] = $this->calcElementPosition();
        $this->webBrowserEngine->printInput($name, $height, $width);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function make(): string
    {
        $submit = '<ReactSubmitButton>Отправить</ReactSubmitButton>';
        [$height, $width] = $this->calcElementPosition();
        $this->webBrowserEngine->printButton($submit, $height, $width);

        $this->webBrowserEngine->enableDisplay();

        return $this->webBrowserEngine->compile();
    }

    /**
     * @inheritDoc
     */
    public function reset(): self
    {
        $this->webBrowserEngine->disableDisplay();
        $this->webBrowserEngine->clearDisplay();
        $this->webBrowserEngine->enableDisplay();

        return $this;
    }

    /**
     * @return float[]|int[]
     */
    private function calcElementPosition(): array
    {
        $width  = $this->webBrowserEngine->getDisplayWidth();

        $lastX = $this->webBrowserEngine->getLastElementCoordinateX();
        $lastY = $this->webBrowserEngine->getLastElementCoordinateY() / $width;

        return [$lastX, $lastY];
    }

    /**
     * @param bool $required
     * @return string
     */
    private function makeRequired(bool $required): string
    {
        return $required ? 'required' : '';
    }
}
