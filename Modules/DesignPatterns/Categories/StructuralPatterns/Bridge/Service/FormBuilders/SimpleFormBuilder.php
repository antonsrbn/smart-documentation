<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\FormBuilders;


use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\BaseFormBuilder;
use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\IWebBrowserEngine;

/**
 * Class SimpleFormBuilder.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\FormBuilders
 */
class SimpleFormBuilder extends BaseFormBuilder
{
    /**
     * SimpleFormBuilder constructor.
     *
     * @param IWebBrowserEngine $webBrowserEngine
     */
    public function __construct(IWebBrowserEngine $webBrowserEngine)
    {
        parent::__construct($webBrowserEngine);

        $this->webBrowserEngine->loadRemoteFiles();
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title): self
    {
        $title = '<SimpleHeader>' . $title . '</SimpleHeader>';

        [$height, $width] = $this->calcElementPositionFromLast();
        $this->webBrowserEngine->printText($title, $height, $width, 12);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addTextField(string $name, string $id, bool $isRequired = false): self
    {
        $text = "<SimpleTextField {$this->makeRequired($isRequired)} id ='{$id}'>" . $name . '</SimpleTextField>';
        $this->webBrowserEngine->printInputWithAuthPosition($text);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addCheckboxField(string $name, string $id, bool $isRequired = false): self
    {
        $checkBox = "<SimpleCheckBoxField {$this->makeRequired($isRequired)} id ='{$id}'>"  . $name . '</SimpleCheckBoxField>';

        $this->webBrowserEngine->printInputWithAuthPosition($checkBox);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addTexAreaField(string $name, string $id, bool $isRequired = false): self
    {
        $textArea = "<SimpleTextAreaField {$this->makeRequired($isRequired)} id ='{$id}'>"  . $name . '</SimpleTextAreaField>';

        $this->webBrowserEngine->printInputWithAuthPosition($textArea);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function make(): string
    {
        $submit = "<SimpleSubmitButton id ='submit'>" . 'Отправить' . '</SimpleSubmitButton>';
        $this->webBrowserEngine->printSubmit($submit);

        return $this->webBrowserEngine->compile();
    }

    /**
     * @inheritDoc
     */
    public function reset(): self
    {
        $this->webBrowserEngine->clearDisplay();

        return $this;
    }

    /**
     * @return float[]|int[]
     */
    private function calcElementPositionFromLast(): array
    {
        $lastX = $this->webBrowserEngine->getLastElementCoordinateX();
        $lastY = $this->webBrowserEngine->getLastElementCoordinateY() + 12;

        return [$lastX, $lastY];
    }

    /**
     * @param bool $required
     * @return string
     */
    private function makeRequired(bool $required): string
    {
        return $required ? 'required=\'true\'' : 'required=\'false\'';
    }
}
