<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service;


use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors\FeedBackFormDirector;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\Factories\Directors\ReviewFormDirector;
use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\IFormBuilder;
use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\FormBuilders\ReactFormBuilder;
use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\FormBuilders\SimpleFormBuilder;
use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\WebBrowserEngines\MozillaEngine;
use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\WebBrowserEngines\WebkitEngine;
use Modules\DesignPatterns\Service\IClientCode;
use Modules\DesignPatterns\Service\Report;

/**
 * Class BridgeClientCode.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service
 */
class BridgeClientCode implements IClientCode
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'bridge';
    }

    /**
     * @inheritDoc
     */
    public function main(): void
    {
        $mozillaEngine = new MozillaEngine();
        $webkitEngine  = new WebkitEngine();

        $reactFormBuilder  = new ReactFormBuilder($mozillaEngine);
        $simpleFormBuilder = new SimpleFormBuilder($mozillaEngine);

        $feedBackFormDirector = new FeedBackFormDirector();
        $reviewFormDirector   = new ReviewFormDirector();

        $feedBackFormDirector->build($reactFormBuilder);
        $this->showForm($reactFormBuilder);

        $reviewFormDirector->build($simpleFormBuilder);
        $this->showForm($simpleFormBuilder);

        $reactFormBuilder->changeWebBrowserEngine($webkitEngine);
        $simpleFormBuilder->changeWebBrowserEngine($webkitEngine);

        $feedBackFormDirector->build($reactFormBuilder);
        $this->showForm($reactFormBuilder);

        $reviewFormDirector->build($simpleFormBuilder);
        $this->showForm($simpleFormBuilder);
    }

    /**
     * @param IFormBuilder $formBuilder
     */
    private function showForm(IFormBuilder $formBuilder): void
    {
        Report::log($formBuilder->make());
    }
}
