<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\WebBrowserEngines;


use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\IWebBrowserEngine;

/**
 * Class MozillaEngine.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\WebBrowserEngines
 */
class MozillaEngine implements IWebBrowserEngine
{
    private array $operation;
    private int   $lastX;
    private int   $lastY;

    /**
     * MozillaEngine constructor.
     */
    public function __construct()
    {
        $this->lastX = 0;
        $this->lastY = 0;

        $this->operation   = [];
        $this->operation[] = 'Mozilla: init memory..';
        $this->operation[] = 'Mozilla: check files..';
        $this->operation[] = 'Mozilla: load GPU Drivers..';
    }

    /**
     * @inheritDoc
     */
    public function getDisplayWidth(): int
    {
        return 2356;
    }

    /**
     * @inheritDoc
     */
    public function getDisplayHeight(): int
    {
        return 863;
    }

    /**
     * @inheritDoc
     */
    public function loadRemoteFiles(): void
    {
        $this->loadRemoteFilesFromCache();
        $this->operation[] = 'Mozilla: download files from server';
    }

    /**
     * @inheritDoc
     */
    public function loadRemoteFilesFromCache(): void
    {
        $this->operation[] = 'Mozilla: download files from cache';
    }

    /**
     * @inheritDoc
     */
    public function printText(string $text, int $x, int $y, int $fontSize): void
    {
        $this->operation[] = "Mozilla: printText: {$text} x:{$x} y:{$y} fontSize: {$fontSize}";
        $this->setNewLastCoordinate($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function printBoldText(string $text, int $x, int $y): void
    {
        $this->operation[] = "Mozilla: printBoldText: {$text} x:{$x} y:{$y} fontSize: 20 bold:1.4 em";
        $this->setNewLastCoordinate($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function printButton(string $text, int $x, int $y): void
    {
        $this->operation[] = "Mozilla: printButton: {$text} x:{$x} y:{$y}";
        $this->setNewLastCoordinate($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function printInput(string $text, int $x, int $y): void
    {
        $this->operation[] = "Mozilla: printInput: {$text} x:{$x} y:{$y}";
        $this->setNewLastCoordinate($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function printInputWithAuthPosition(string $text): void
    {
        $this->operation[] = "Mozilla: printInputWithAuthPosition: {$text}";
        $this->setNewLastCoordinate($this->lastX + 7, $this->lastY);
    }

    /**
     * @inheritDoc
     */
    public function printSubmit(string $text): void
    {
        $this->operation[] = "Mozilla: printSubmit: {$text}";

        $this->setNewLastCoordinate($this->lastX + 24, $this->lastY);
    }

    /**
     * @inheritDoc
     */
    public function compile(): string
    {
        $this->operation[] = "Mozilla: render in GPU..";
        $result            = implode("\n", $this->operation);
        $this->operation   = [];

        $this->lastX = 0;
        $this->lastY = 0;

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function disableDisplay(): void
    {
        $this->operation[] = 'Mozilla: disableDisplay';
    }

    /**
     * @inheritDoc
     */
    public function clearDisplay(): void
    {
        $this->operation = [];

        $this->setNewLastCoordinate(0, 0);

        $this->operation[] = 'Mozilla: clearDisplay';
    }

    /**
     * @inheritDoc
     */
    public function enableDisplay(): void
    {
        $this->operation[] = 'Mozilla: enableDisplay';
    }

    /**
     * @inheritDoc
     */
    public function getLastElementCoordinateX(): int
    {
        return $this->lastX;
    }

    /**
     * @inheritDoc
     */
    public function getLastElementCoordinateY(): int
    {
        return $this->lastY;
    }

    /**
     * @param int $x
     * @param int $y
     */
    private function setNewLastCoordinate(int $x, int $y): void
    {
        $this->operation[] = "Mozilla: setNewLastCoordinate : x:{$x} y:{$y}";
        $this->lastX       = $x;
        $this->lastY       = $y;
    }
}
