<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\WebBrowserEngines;


use Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\IWebBrowserEngine;


/**
 * Class WebkitEngine.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service\WebBrowserEngines
 */
class WebkitEngine implements IWebBrowserEngine
{
    private array $operation;
    private int   $lastX;
    private int   $lastY;

    /**
     * WebkitEngine constructor.
     */
    public function __construct()
    {
        $this->lastX = 0;
        $this->lastY = 0;

        $this->operation   = [];
        $this->operation[] = 'Webkit: init memory..';
    }

    /**
     * @inheritDoc
     */
    public function getDisplayWidth(): int
    {
        return 2464;
    }

    /**
     * @inheritDoc
     */
    public function getDisplayHeight(): int
    {
        return 1153;
    }

    /**
     * @inheritDoc
     */
    public function loadRemoteFiles(): void
    {
        $this->operation[] = 'Webkit: download files from server';
    }

    /**
     * @inheritDoc
     */
    public function loadRemoteFilesFromCache(): void
    {
        $this->operation[] = 'Webkit: download files from cache';
    }

    /**
     * @inheritDoc
     */
    public function printText(string $text, int $x, int $y, int $fontSize): void
    {
        $this->operation[] = "Webkit: printText: {$text} x:{$x} y:{$y} fontSize: {$fontSize}";
        $this->setNewLastCoordinate($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function printBoldText(string $text, int $x, int $y): void
    {
        $this->operation[] = "Webkit: printBoldText: {$text} x:{$x} y:{$y} fontSize: 24 bold:2";
        $this->setNewLastCoordinate($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function printButton(string $text, int $x, int $y): void
    {
        $this->operation[] = "Webkit: printButton: {$text} x:{$x} y:{$y}";
        $this->setNewLastCoordinate($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function printInput(string $text, int $x, int $y): void
    {
        $this->operation[] = "Webkit: printInput: {$text} x:{$x} y:{$y}";
        $this->setNewLastCoordinate($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function printInputWithAuthPosition(string $text): void
    {
        $this->operation[] = "Webkit: printInputWithAuthPosition: {$text}";
        $this->setNewLastCoordinate($this->lastX + 12, $this->lastY);
    }

    /**
     * @inheritDoc
     */
    public function printSubmit(string $text): void
    {
        $this->operation[] = "Webkit: printSubmit: {$text}";

        $this->setNewLastCoordinate($this->lastX + 24, $this->lastY);
    }

    /**
     * @inheritDoc
     */
    public function compile(): string
    {
        $this->operation[] = 'Webkit: start render..';
        $this->operation[] = 'Webkit: start render ready';
        $result            = implode("\n", $this->operation);
        $this->reloadInit();

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function disableDisplay(): void
    {
        $this->operation[] = 'Webkit: disableDisplay';
    }

    /**
     * @inheritDoc
     */
    public function clearDisplay(): void
    {
        $this->reloadInit();

        $this->operation[] = 'Webkit: clearDisplay';
    }

    /**
     * @inheritDoc
     */
    public function enableDisplay(): void
    {
        $this->operation[] = 'Webkit: enableDisplay';
    }

    /**
     * @inheritDoc
     */
    public function getLastElementCoordinateX(): int
    {
        return $this->lastX;
    }

    /**
     * @inheritDoc
     */
    public function getLastElementCoordinateY(): int
    {
        return $this->lastY;
    }

    /**
     * @param int $x
     * @param int $y
     */
    private function setNewLastCoordinate(int $x, int $y): void
    {
        $this->operation[] = "Webkit: setNewLastCoordinate : x:{$x} y:{$y}";
        $this->lastX       = $x;
        $this->lastY       = $y;
    }

    private function reloadInit(): void
    {
        $this->setNewLastCoordinate(0, 0);

        $this->operation = [];

        $this->operation[] = "Webkit: reload init..";
    }
}
