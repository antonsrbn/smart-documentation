<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service;

use Modules\DesignPatterns\Categories\CreationalPatterns\Builder\Service\IFormBuilder;

/**
 * Class BaseFormBuilder.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service
 */
abstract class BaseFormBuilder implements IFormBuilder
{
    protected IWebBrowserEngine $webBrowserEngine;

    /**
     * DataInterface constructor.
     *
     * @param IWebBrowserEngine webBrowserEngine
     */
    public function __construct(IWebBrowserEngine $webBrowserEngine)
    {
        $this->webBrowserEngine = $webBrowserEngine;
    }

    /**
     * @param IWebBrowserEngine $webBrowserEngine
     * @return $this
     */
    public function changeWebBrowserEngine(IWebBrowserEngine $webBrowserEngine): self
    {
        $this->reset();

        $this->webBrowserEngine = $webBrowserEngine;

        return $this;
    }
}
