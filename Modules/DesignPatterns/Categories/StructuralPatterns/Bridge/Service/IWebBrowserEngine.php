<?php


namespace Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service;

/**
 * Interface IWebBrowserEngine.
 *
 * @package Modules\DesignPatterns\Categories\StructuralPatterns\Bridge\Service
 */
interface IWebBrowserEngine
{
    /**
     * @return int
     */
    public function getDisplayWidth(): int;

    /**
     * @return int
     */
    public function getDisplayHeight(): int;

    /**
     * @return void
     */
    public function loadRemoteFiles(): void;

    /**
     * @return void
     */
    public function loadRemoteFilesFromCache(): void;

    /**
     * @return int
     */
    public function getLastElementCoordinateX(): int;

    /**
     * @return int
     */
    public function getLastElementCoordinateY(): int;

    /**
     * @param string $text
     * @param int    $x
     * @param int    $y
     * @param int    $fontSize
     */
    public function printText(string $text, int $x, int $y, int $fontSize): void;

    /**
     * @param string $text
     * @param int    $x
     * @param int    $y
     */
    public function printBoldText(string $text, int $x, int $y): void;

    /**
     * @param string $text
     * @param int    $x
     * @param int    $y
     */
    public function printButton(string $text, int $x, int $y): void;

    /**
     * @param string $text
     * @param int    $x
     * @param int    $y
     */
    public function printInput(string $text, int $x, int $y): void;

    /**
     * @param string $text
     */
    public function printInputWithAuthPosition(string $text): void;

    /**
     * @param string $text
     */
    public function printSubmit(string $text): void;

    /**
     * @return string
     */
    public function compile(): string;

    /**
     *
     */
    public function disableDisplay(): void;

    /**
     *
     */
    public function clearDisplay(): void;

    /**
     *
     */
    public function enableDisplay(): void;
}
