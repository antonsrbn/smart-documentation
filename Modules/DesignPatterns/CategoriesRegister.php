<?php


namespace Modules\DesignPatterns;

use Modules\DesignPatterns\Categories\CreationalPatterns\CreationalPatternsCategory;
use Modules\DesignPatterns\Categories\StructuralPatterns\StructuralPatterns;

/**
 * Class CategoriesRegister.
 *
 * @package Modules\DesignPatterns
 */
abstract class CategoriesRegister
{
    public const CATEGORIES = [
        CreationalPatternsCategory::class,
        StructuralPatterns::class,
    ];
}
