<?php /** @noinspection PhpHierarchyChecksInspection */


namespace Modules\DesignPatterns\Commands;


use Exception;
use Illuminate\Console\Command;
use Modules\DesignPatterns\Service\ClientCodeRunner;

/**
 * Class ClientCodeListCommand.
 *
 * @package Modules\DesignPatterns\Commands
 */
class ClientCodeListCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     */
    protected $signature = 'client-code:list';

    /**
     * The console command description.
     *
     */
    protected $description = 'List client codes';

    /**
     * Execute the console command.
     *
     * @param ClientCodeRunner $clientCodeRunner
     * @throws Exception
     */
    public function handle(ClientCodeRunner $clientCodeRunner): void
    {
        $list = $clientCodeRunner->getList();
        $this->showList($list);
    }

    /**
     * @param array $list
     */
    private function showList(array $list): void
    {
        foreach ($list as $item) {
            $this->warn(' - ' . $item['docName'] . ':');
            foreach ($item['clientCodes'] as $clientCode) {
                $this->info("\t" . ' - ' . $clientCode['docName']);
                $this->info("\t\t" . '--category=' . $item['name'] . ' --clientCode=' . $clientCode['name'] . "\n");
            }

        }
    }

}
