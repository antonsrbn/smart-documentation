<?php /** @noinspection PhpHierarchyChecksInspection */


namespace Modules\DesignPatterns\Commands;


use Exception;
use Illuminate\Console\Command;
use Modules\DesignPatterns\Service\Report;
use Modules\DesignPatterns\Service\ClientCodeRunner;

/**
 * Class ClientCodeRunCommand.
 *
 * @package Modules\DesignPatterns\Commands
 */
class ClientCodeRunCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     */
    protected $signature = 'client-code:run {--category=} {--clientCode=}';

    /**
     * The console command description.
     *
     */
    protected $description = 'Run client codes';

    /**
     * Execute the console command.
     *
     * @param ClientCodeRunner $clientCodeRunner
     * @throws Exception
     */
    public function handle(ClientCodeRunner $clientCodeRunner): void
    {
        $category   = $this->option('category');
        $clientCode = $this->option('clientCode');

        $this->reportInit();

        $clientCodeRunner->run($category, $clientCode);
    }

    /**
     * @throws Exception
     */
    private function reportInit(): void
    {
        Report::init(function () {
            return $this;
        }, $this);
    }
}
