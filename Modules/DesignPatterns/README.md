# Паттерны проектирования

## Смысл

Паттерн проектирования — это часто встречающееся решение определённой проблемы при проектировании архитектуры программ.

## Ссылки

+ [Порождающие паттерны](Categories/CreationalPatterns/README.md);
+ [Структурные паттерны](Categories/StructuralPatterns/README.md);
+ [Поведенческие паттерны](Categories/BehavioralPatterns/README.md).

### Команды

Список паттернов: `php artisan client-code:list`   
Команда запуска результата: `php artisan client-code:run --category=[Категория] --clientCode=[Название паттерна]`

