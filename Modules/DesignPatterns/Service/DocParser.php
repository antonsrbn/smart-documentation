<?php


namespace Modules\DesignPatterns\Service;


use Exception;
use Modules\DesignPatterns\Dto\DocFile;
use Modules\DesignPatterns\Dto\IDocFile;
use ReflectionClass;
use ReflectionException;

/**
 * Class DocParser.
 *
 * @package Modules\DesignPatterns\Service
 */
class DocParser
{
    private const DOC_FILENAME = 'README.md';

    /**
     * @param $object
     * @return IDocFile|null
     * @throws ReflectionException
     * @throws Exception
     */
    public function parse($object): ?IDocFile
    {
        $fileDocFilePath = $this->getDocFilePath($object);
        if ($fileDocFilePath !== null) {

            return $this->parseDocFile($fileDocFilePath);
        }

        return null;
    }

    /**
     * @param $object
     * @return string|null
     * @throws ReflectionException
     */
    private function getDocFilePath($object): ?string
    {
        $className = get_class($object);

        $reflection = new ReflectionClass($className);
        $fileName   = $reflection->getFileName();

        return $this->findDocFilePath($fileName);
    }

    /**
     * @param string $fileName
     * @return string|null
     */
    private function findDocFilePath(string $fileName): ?string
    {
        $filePathArray = explode('/', $fileName);
        array_pop($filePathArray);

        while ($filePathArray !== []) {
            $filePath    = implode('/', $filePathArray);
            $docFilePath = $filePath . '/' . self::DOC_FILENAME;

            if (file_exists($docFilePath)) {
                return $docFilePath;
            }


            array_pop($filePathArray);
        }

        return null;
    }

    /**
     * @param string $docFilePath
     * @return IDocFile
     * @throws Exception
     */
    private function parseDocFile(string $docFilePath): IDocFile
    {
        $textArray = [];
        $handle    = @fopen($docFilePath, 'rb');

        while (($buffer = fgets($handle, 4096)) !== false) {
            $buffer = trim($buffer);

            $textArray[] = $buffer;

        }

        if (!feof($handle)) {
            throw new Exception("fgets() for filePath $docFilePath is failed unexpectedly.");
        }

        fclose($handle);

        return $this->parseDocFileArrayText($textArray);
    }

    /**
     * @param array $textArray
     * @return IDocFile
     */
    private function parseDocFileArrayText(array $textArray): IDocFile
    {
        $name = '';

        foreach ($textArray as $row) {
            $matches = [];

            if (preg_match_all('/^#\s(.+)?/ui', $row, $matches)) {
                $name = $matches[1][0];
            }

        }

        $docFie = new DocFile();

        $docFie->setName($name);

        return $docFie;
    }
}
