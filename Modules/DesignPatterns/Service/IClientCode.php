<?php


namespace Modules\DesignPatterns\Service;

/**
 * Interface IClientCode.
 *
 * @package Modules\DesignPatterns\Service
 */
interface IClientCode extends IIdentificationMethod
{
    /**
     *
     */
    public function main(): void;
}
