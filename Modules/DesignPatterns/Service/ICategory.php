<?php


namespace Modules\DesignPatterns\Service;

/**
 * Interface ICategory.
 *
 * @package Modules\DesignPatterns\Service
 */
interface ICategory extends IIdentificationMethod
{
    /**
     * @return string[]
     */
    public function getClientCodes(): array;
}
