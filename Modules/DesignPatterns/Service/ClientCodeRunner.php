<?php


namespace Modules\DesignPatterns\Service;

use Exception;
use Modules\DesignPatterns\CategoriesRegister;
use ReflectionException;

/**
 * Class ClientCodeRunner.
 *
 * @package Modules\DesignPatterns\Service
 */
class ClientCodeRunner
{
    /**
     * @var ICategory[]
     */
    private array $categories;
    /**
     * @var DocParser
     */
    private DocParser $docParser;

    /**
     * ClientCodeRunner constructor.
     *
     * @param DocParser $docParser
     */
    public function __construct(DocParser $docParser)
    {
        $this->categories = $this->initCategories(CategoriesRegister::CATEGORIES);
        $this->docParser  = $docParser;
    }

    /**
     * @return array
     * @throws Exception
     * @throws ReflectionException
     */
    public function getList(): array
    {
        $list = [];

        foreach ($this->categories as $category) {
            $clientCodes    = $this->initClientCodes($category);
            $clientCodeList = [];

            foreach ($clientCodes as $clientCode) {
                $docFile = $this->docParser->parse($clientCode);

                $clientCodeList[] = [
                    'docName' => $docFile->getName(),
                    'name'    => $clientCode->getName(),
                ];
            }

            $docFile = $this->docParser->parse($category);

            $list[] = [
                'docName'     => $docFile->getName(),
                'name'        => $category->getName(),
                'clientCodes' => $clientCodeList,
            ];
        }

        return $list;
    }

    /**
     * @param string $categoryName
     * @param string $clientCodeName
     * @throws Exception
     */
    public function run(string $categoryName, string $clientCodeName): void
    {
        $category = $this->categories[$categoryName];
        if ($category === null) {
            throw new Exception('Category not found.');
        }

        $clientCodes = $this->initClientCodes($category);

        $clientCode = $clientCodes[$clientCodeName];

        if ($clientCode === null) {
            throw new Exception('ClientCode not found.');
        }

        $this->showHeader($category, $clientCode);

        $clientCode->main();
    }

    /**
     * @param array $categories
     * @return ICategory[]
     */
    private function initCategories(array $categories): array
    {
        $initCategories = [];
        foreach ($categories as $category) {
            /**
             * @var $initCategory ICategory
             */
            $initCategory                             = new $category;
            $initCategories[$initCategory->getName()] = $initCategory;
        }

        return $initCategories;
    }

    /**
     * @param ICategory $category
     * @return IClientCode[]
     */
    private function initClientCodes(ICategory $category): array
    {
        $initClientCodes = [];
        foreach ($category->getClientCodes() as $clientCode) {
            /**
             * @var $clientCode IClientCode
             */
            $initClientCode                              = new $clientCode;
            $initClientCodes[$initClientCode->getName()] = $initClientCode;
        }

        return $initClientCodes;
    }

    /**
     * @param ICategory   $category
     * @param IClientCode $clientCode
     * @throws Exception
     * @throws ReflectionException
     */
    private function showHeader(ICategory $category, IClientCode $clientCode): void
    {
        $docFileCategory   = $this->docParser->parse($category);
        $docFileClientCode = $this->docParser->parse($clientCode);

        Report::head($docFileCategory->getName() . ': ' . $docFileClientCode->getName() .
            ".\nРезультат кода:");
    }
}
