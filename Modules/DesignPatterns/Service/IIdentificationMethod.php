<?php


namespace Modules\DesignPatterns\Service;

/**
 * Interface IIdentificationMethod.
 *
 * @package Modules\DesignPatterns\Service
 */
interface IIdentificationMethod
{
    /**
     * @return string
     */
    public function getName(): string;
}
