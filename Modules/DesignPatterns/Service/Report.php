<?php /** @noinspection PhpMissingFieldTypeInspection */


namespace Modules\DesignPatterns\Service;


use Closure;
use Exception;

/**
 * Class Report.
 *
 * @package Modules\DesignPatterns\Service
 */
class Report
{
    /**
     * @var self
     */
    private static $instance;
    /**
     * @var Closure
     */
    private Closure $closure;
    private         $context;

    /**
     * Report constructor.
     *
     * @param Closure $closure
     * @param         $context
     */
    protected function __construct(Closure $closure, $context)
    {
        $this->closure = $closure;
        $this->context = $context;
    }

    /**
     * @param string $text
     */
    protected function warn(string $text): void
    {
        $this->closure->call($this->context)->warn($text . "\n");
    }

    /**
     * @param string $text
     */
    protected function report(string $text): void
    {
        $this->closure->call($this->context)->info($text . "\n");
    }
    
    /**
     * @param string $text
     * @throws Exception
     */
    public static function head(string $text): void
    {
        if (self::$instance === null) {
            throw new Exception('Closure not init!');
        }

        self::$instance->warn($text);
    }

    /**
     * @param string $text
     * @throws Exception
     */
    public static function log(string $text): void
    {
        if (self::$instance === null) {
            throw new Exception('Closure not init!');
        }

        self::$instance->report($text);
    }

    /**
     * @param Closure $report
     * @param         $context
     * @throws Exception
     */
    public static function init(Closure $report, $context): void
    {
        if (self::$instance !== null) {
            throw new Exception('Closure are is init!');
        }
        self::$instance = new self($report, $context);
    }
}
