<?php

namespace Modules\DesignPatterns;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Modules\DesignPatterns\Commands\ClientCodeListCommand;
use Modules\DesignPatterns\Commands\ClientCodeRunCommand;

/**
 * Class DesignPatternsServiceProvider
 * Провайдер модуля DesignPatterns.
 *
 * @package Modules\DesignPatterns.
 */
class DesignPatternsServiceProvider extends BaseServiceProvider
{

    /**
     * Точка входа в модуль.
     *
     * @param Router $router
     * @noinspection PhpUnusedParameterInspection
     */
    public function boot(Router $router): void
    {
        $this->commands([
            ClientCodeRunCommand::class,
            ClientCodeListCommand::class
        ]);
    }
}
