<?php


namespace Modules\DesignPatterns\Dto;

/**
 * Class DocFile.
 *
 * @package Modules\DesignPatterns\Dto
 */
class DocFile implements IDocFile
{
    private string $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
