<?php


namespace Modules\DesignPatterns\Dto;

/**
 * Interface IDocFile.
 *
 * @package Modules\DesignPatterns\Dto
 */
interface IDocFile
{
    /**
     * @return string
     */
    public function getName(): string;
}
